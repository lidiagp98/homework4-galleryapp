package com.applaudotrainee.galleryapp.domain.interaction

import com.applaudotrainee.galleryapp.domain.repository.PhotoRepository

class GetPhotosUseCaseImpl(private val photoRepository: PhotoRepository) : GetPhotosUseCase {
    override operator fun invoke() = photoRepository.getPhotoPreview()
}