package com.applaudotrainee.galleryapp.domain.interaction

import com.applaudotrainee.galleryapp.domain.model.Photo
import io.reactivex.rxjava3.core.Flowable

interface GetPhotosFromApi {
    operator fun invoke(): Flowable<List<Photo>>
}