package com.applaudotrainee.galleryapp.domain.util

enum class PhotoApiStatus { LOADING, ERROR, DONE, NO_CONNECTION }