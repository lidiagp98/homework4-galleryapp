package com.applaudotrainee.galleryapp.domain.interaction

import com.applaudotrainee.galleryapp.domain.repository.PhotoRepository

class GetPhotoDetailsUseCaseImpl(private val photoRepository: PhotoRepository) :
    GetPhotoDetailsUseCase {
    override operator fun invoke(photoId: Int) = photoRepository.getPhotoDetails(photoId)
}