package com.applaudotrainee.galleryapp.domain.model

data class PhotoPreviewEntity(
    val idPhoto: Int,
    val thumbnailUrl: String,
    val title: String
)
