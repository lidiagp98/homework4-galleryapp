package com.applaudotrainee.galleryapp.domain.interaction

import com.applaudotrainee.galleryapp.domain.model.Photo
import com.applaudotrainee.galleryapp.domain.repository.PhotoRepository
import io.reactivex.rxjava3.core.Flowable

class GetPhotosFromApiImpl(private val photoRepository: PhotoRepository) : GetPhotosFromApi {
    override fun invoke(): Flowable<List<Photo>> = photoRepository.getPhotosFromRemote()
}