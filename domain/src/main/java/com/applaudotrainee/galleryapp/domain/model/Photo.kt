package com.applaudotrainee.galleryapp.domain.model

data class Photo(
    val albumId: Int,
    val idPhoto: Int,
    val title: String,
    val url: String,
    val thumbnailUrl: String
)