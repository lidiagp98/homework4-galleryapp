package com.applaudotrainee.galleryapp.domain.di

import com.applaudotrainee.galleryapp.domain.interaction.*
import org.koin.dsl.module

val interactionModule = module {
    factory<GetPhotosUseCase> { GetPhotosUseCaseImpl(get()) }
    factory<GetPhotoDetailsUseCase> { GetPhotoDetailsUseCaseImpl(get()) }
    factory<GetPhotosFromApi> { GetPhotosFromApiImpl(get()) }
}