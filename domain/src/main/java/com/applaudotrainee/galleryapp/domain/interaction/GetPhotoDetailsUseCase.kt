package com.applaudotrainee.galleryapp.domain.interaction

import com.applaudotrainee.galleryapp.domain.model.Photo
import io.reactivex.rxjava3.core.Flowable

interface GetPhotoDetailsUseCase {
    operator fun invoke(photoId: Int): Flowable<Photo>
}