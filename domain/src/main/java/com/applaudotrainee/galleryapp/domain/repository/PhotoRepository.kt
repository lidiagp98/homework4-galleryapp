package com.applaudotrainee.galleryapp.domain.repository

import com.applaudotrainee.galleryapp.domain.model.Photo
import com.applaudotrainee.galleryapp.domain.model.PhotoPreviewEntity
import io.reactivex.rxjava3.core.Flowable

interface PhotoRepository {
    fun getPhotoPreview(): Flowable<List<PhotoPreviewEntity>>
    fun getPhotosFromRemote(): Flowable<List<Photo>>
    fun getPhotoDetails(photoId: Int): Flowable<Photo>
}