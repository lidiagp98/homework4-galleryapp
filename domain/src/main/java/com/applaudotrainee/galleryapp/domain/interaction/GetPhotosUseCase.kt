package com.applaudotrainee.galleryapp.domain.interaction

import com.applaudotrainee.galleryapp.domain.model.PhotoPreviewEntity
import io.reactivex.rxjava3.core.Flowable

interface GetPhotosUseCase {
    operator fun invoke(): Flowable<List<PhotoPreviewEntity>>
}