package com.applaudotrainee.galleryapp.domain

import com.applaudotrainee.galleryapp.domain.interaction.GetPhotosFromApiImpl
import com.applaudotrainee.galleryapp.domain.repository.PhotoRepository
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import org.mockito.Mockito.`when`

class GetPhotosFromApiImplTest() {
    private val photoRepositoryMock = Mockito.mock(PhotoRepository::class.java)
    private val getPhotosFromApiUseCase = GetPhotosFromApiImpl(photoRepositoryMock)

    @Before
    fun mockFunctionality() {
        `when`(photoRepositoryMock.getPhotosFromRemote()).thenReturn(photoListFlowable)
    }

    @Test
    fun getPhotosFromApi_success_returnPhoto() {
        getPhotosFromApiUseCase().test().assertValue(photoList)
    }
}