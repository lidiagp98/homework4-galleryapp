package com.applaudotrainee.galleryapp.domain

import com.applaudotrainee.galleryapp.domain.interaction.GetPhotosUseCaseImpl
import com.applaudotrainee.galleryapp.domain.repository.PhotoRepository
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import org.mockito.Mockito.`when`

class GetPhotosUseCaseImplTest() {
    private val photoRepositoryMock = Mockito.mock(PhotoRepository::class.java)
    private val getPhotosUseCase = GetPhotosUseCaseImpl(photoRepositoryMock)

    @Before
    fun mockFunctionality() {
        `when`(photoRepositoryMock.getPhotoPreview()).thenReturn(photoPreviewListFlowable)
    }

    @Test
    fun getPhotoPreview_success_returnPhoto() {
        getPhotosUseCase().test().assertValue(photoPreviewList)
    }
}