package com.applaudotrainee.galleryapp.domain

import com.applaudotrainee.galleryapp.domain.interaction.GetPhotoDetailsUseCaseImpl
import com.applaudotrainee.galleryapp.domain.repository.PhotoRepository
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock

class GetPhotoDetailsUseCaseImplTest() {

    private val photoRepositoryMock = mock(PhotoRepository::class.java)
    private val getPhotoDetailsUseCase = GetPhotoDetailsUseCaseImpl(photoRepositoryMock)

    @Before
    fun mockFunctionality() {
        `when`(photoRepositoryMock.getPhotoDetails(PHOTO_ID)).thenReturn(photoFlowable)
    }

    @Test
    fun getPhotoDetails_success_returnPhoto() {
        getPhotoDetailsUseCase(PHOTO_ID).test().assertValue(photo)
    }
}