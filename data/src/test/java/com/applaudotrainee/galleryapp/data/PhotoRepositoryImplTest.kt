package com.applaudotrainee.galleryapp.data

import com.applaudotrainee.galleryapp.data.database.dao.PhotoDao
import com.applaudotrainee.galleryapp.data.repository.PhotoRepositoryImpl
import com.applaudotrainee.galleryapp.data.service.PhotosAPI
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock

class PhotoRepositoryImplTest {

    private val photoDaoMock = mock(PhotoDao::class.java)
    private val photoApiMock = mock(PhotosAPI::class.java)
    private val photoRepositoryImplMock = PhotoRepositoryImpl(photoDaoMock, photoApiMock)

    @Before
    fun mockFunctionality() {
        `when`(photoDaoMock.getAllPreviewPhotos()).thenReturn(photoPreviewListDataFlowable)
        `when`(photoDaoMock.getPhotoDetails(PHOTO_ID_1)).thenReturn(photoDataDetailsFlowable)
        `when`(photoApiMock.getPhotos()).thenReturn(photoListDataFlowable)
        `when`(photoDaoMock.insertPhotos(photoListData)).then { photoDatabase.addAll(photoListData) }
    }

    @Test
    fun getPhotoPreview_successful_returnsListOfPhotoPreview() {
        photoRepositoryImplMock.getPhotoPreview().test().assertValue(photoListDomain)
    }

    @Test
    fun getPhotoDetails_successful_returnPhoto() {
        photoRepositoryImplMock.getPhotoDetails(PHOTO_ID_1).test().assertValue(photoDetailToDomain)
    }

    @Test
    fun getPhotos_successful_returnDoneStatus() {
        photoRepositoryImplMock.getPhotosFromRemote().test()
        assertEquals(photoDatabase, photoListData)
    }
}