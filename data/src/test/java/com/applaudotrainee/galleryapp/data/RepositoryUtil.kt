package com.applaudotrainee.galleryapp.data

import com.applaudotrainee.galleryapp.data.database.model.PhotoData
import com.applaudotrainee.galleryapp.data.database.model.PhotoPreviewData
import io.reactivex.rxjava3.core.Flowable


const val PHOTO_ID_1 = 1
const val PHOTO_ID_2 = 2
const val PHOTO_ID_3 = 3
const val URL_PHOTO_1 = "http://photo1.com/"
const val URL_PHOTO_2 = "http://photo2.com/"
const val URL_PHOTO_3 = "http://photo3.com/"
const val TITLE_URL_1 = "photo1"
const val TITLE_URL_2 = "photo2"
const val TITLE_URL_3 = "photo3"


val photoPreviewListData = listOf(
    PhotoPreviewData(
        PHOTO_ID_1, URL_PHOTO_1, TITLE_URL_1
    ),
    PhotoPreviewData(
        PHOTO_ID_2, URL_PHOTO_2, TITLE_URL_2
    ),
    PhotoPreviewData(
        PHOTO_ID_3, URL_PHOTO_3, TITLE_URL_3
    )
)

val photoPreviewListDataFlowable: Flowable<List<PhotoPreviewData>> = Flowable.just(photoPreviewListData)

val photoListDomain =
    photoPreviewListData.map { photoPreviewData -> photoPreviewData.mapToDomainModel() }

val photoListData = listOf(
    PhotoData(
        1, PHOTO_ID_1, TITLE_URL_1, URL_PHOTO_1, URL_PHOTO_1
    ),
    PhotoData(
        1, PHOTO_ID_2, TITLE_URL_2, URL_PHOTO_2, TITLE_URL_2
    ),
    PhotoData(
        1, PHOTO_ID_2, TITLE_URL_2, URL_PHOTO_2, TITLE_URL_2
    )
)

val photoListDataFlowable: Flowable<List<PhotoData>> = Flowable.just(photoListData)

val photoDataDetailsFlowable: Flowable<PhotoData> = Flowable.just(photoListData.first())

val photoDatabase = mutableListOf<PhotoData>()

val photoDetailToDomain = photoListData.first().mapToDomainModel()