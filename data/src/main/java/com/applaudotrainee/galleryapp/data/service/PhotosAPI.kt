package com.applaudotrainee.galleryapp.data.service

import com.applaudotrainee.galleryapp.data.database.model.PhotoData
import io.reactivex.rxjava3.core.Flowable
import retrofit2.http.GET

interface PhotosAPI {
    companion object {
        private const val ALBUM_ID = 1
        const val PHOTOS_URL = "photos?albumId=$ALBUM_ID"
    }

    @GET(PHOTOS_URL)
    fun getPhotos(): Flowable<List<PhotoData>>
}