package com.applaudotrainee.galleryapp.data.di

import com.applaudotrainee.galleryapp.data.service.PhotosAPI
import org.koin.dsl.module
import retrofit2.CallAdapter
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

private const val BASE_URL = "https://jsonplaceholder.typicode.com/"

val serviceModule = module {
    single { GsonConverterFactory.create() as Converter.Factory }

    single { RxJava3CallAdapterFactory.create() as CallAdapter.Factory }

    single {
        Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addCallAdapterFactory(get())
            .addConverterFactory(get())
            .build()
    }

    single { get<Retrofit>().create(PhotosAPI::class.java) }
}