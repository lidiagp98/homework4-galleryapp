package com.applaudotrainee.galleryapp.data.repository

import com.applaudotrainee.galleryapp.data.database.dao.PhotoDao
import com.applaudotrainee.galleryapp.data.service.PhotosAPI
import com.applaudotrainee.galleryapp.domain.model.Photo
import com.applaudotrainee.galleryapp.domain.model.PhotoPreviewEntity
import com.applaudotrainee.galleryapp.domain.repository.PhotoRepository
import io.reactivex.rxjava3.core.Flowable

class PhotoRepositoryImpl(private val photoDao: PhotoDao, private val photoApi: PhotosAPI) :
    PhotoRepository {

    companion object {
        private const val TOTAL_PHOTOS = 25
    }

    override fun getPhotoPreview(): Flowable<List<PhotoPreviewEntity>> =
        photoDao.getAllPreviewPhotos()
            .map { photoPreviewData -> photoPreviewData.map { it.mapToDomainModel() } }

    override fun getPhotoDetails(photoId: Int): Flowable<Photo> =
        photoDao.getPhotoDetails(photoId).map { it.mapToDomainModel() }

    override fun getPhotosFromRemote(): Flowable<List<Photo>> =
        photoApi.getPhotos()
            .take(TOTAL_PHOTOS.toLong())
            .doOnNext {
                photoDao.deleteAllPhotos()
                photoDao.insertPhotos(it)
            }
            .map { list -> list.map { it.mapToDomainModel() } }

}