package com.applaudotrainee.galleryapp.data.di

import androidx.room.Room
import com.applaudotrainee.galleryapp.data.database.PhotoRoomDatabase
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

private const val DATABASE_NAME = "photo_database"

val databaseModule = module {
    single {
        Room.databaseBuilder(
            androidContext().applicationContext,
            PhotoRoomDatabase::class.java,
            DATABASE_NAME
        ).build()
    }
    factory { get<PhotoRoomDatabase>().photoDao() }
}