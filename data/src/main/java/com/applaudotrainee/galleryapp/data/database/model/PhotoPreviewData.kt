package com.applaudotrainee.galleryapp.data.database.model

import com.applaudotrainee.galleryapp.data.mapper.DomainMapper
import com.applaudotrainee.galleryapp.domain.model.PhotoPreviewEntity

data class PhotoPreviewData(
    val idPhoto: Int,
    val thumbnailUrl: String,
    val title: String
) : DomainMapper<PhotoPreviewEntity> {
    override fun mapToDomainModel() = PhotoPreviewEntity(idPhoto, thumbnailUrl, title)
}
