package com.applaudotrainee.galleryapp.data.di

import com.applaudotrainee.galleryapp.data.repository.PhotoRepositoryImpl
import com.applaudotrainee.galleryapp.domain.repository.PhotoRepository
import org.koin.dsl.module

val repositoryModule = module {
    factory<PhotoRepository> { PhotoRepositoryImpl(get(), get()) }
}