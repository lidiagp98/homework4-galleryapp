package com.applaudotrainee.galleryapp.data.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.applaudotrainee.galleryapp.data.database.model.PhotoData
import com.applaudotrainee.galleryapp.data.database.model.PhotoPreviewData
import io.reactivex.rxjava3.core.Flowable

@Dao
interface PhotoDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertPhotos(photos: List<PhotoData>)

    @Query("DELETE FROM PhotoData")
    fun deleteAllPhotos()

    @Query("SELECT idPhoto, thumbnailUrl, title FROM PhotoData")
    fun getAllPreviewPhotos(): Flowable<List<PhotoPreviewData>>

    @Query("SELECT * FROM PhotoData WHERE idPhoto = :photoId")
    fun getPhotoDetails(photoId: Int): Flowable<PhotoData>
}