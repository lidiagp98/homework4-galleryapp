package com.applaudotrainee.galleryapp.data.database.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.applaudotrainee.galleryapp.data.mapper.DomainMapper
import com.applaudotrainee.galleryapp.domain.model.Photo
import com.google.gson.annotations.SerializedName

@Entity
data class PhotoData(
    @field:SerializedName("albumId") val albumId: Int,
    @field:SerializedName("id") @PrimaryKey val idPhoto: Int,
    @field:SerializedName("title") val title: String,
    @field:SerializedName("url") val url: String,
    @field:SerializedName("thumbnailUrl") val thumbnailUrl: String
) : DomainMapper<Photo> {
    override fun mapToDomainModel() = Photo(albumId, idPhoto, title, url, thumbnailUrl)
}
