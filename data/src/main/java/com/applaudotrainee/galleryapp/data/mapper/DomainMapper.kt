package com.applaudotrainee.galleryapp.data.mapper

interface DomainMapper<T : Any> {
    fun mapToDomainModel(): T
}