package com.applaudotrainee.galeryapp

import com.applaudotrainee.galeryapp.viewmodel.PhotoDetailsViewModel
import com.applaudotrainee.galleryapp.domain.interaction.GetPhotoDetailsUseCase
import com.applaudotrainee.galleryapp.domain.model.Photo
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentCaptor
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations

class PhotoDetailsViewModelTest() {

    private val getPhotoDetailsUseCase = mock(GetPhotoDetailsUseCase::class.java)

    private val photoDetailsViewModel = PhotoDetailsViewModel(getPhotoDetailsUseCase)

    @Before
    fun mockFunctionality() {
        MockitoAnnotations.openMocks(this)
        `when`(getPhotoDetailsUseCase(PHOTO_ID)).thenReturn(photoFlowable)
    }

    @Test
    fun getPhotoPreview_success_returnPhotoDetails() {
        photoDetailsViewModel.getPhotoPreview(PHOTO_ID).test().assertValue(photo)
    }
}