package com.applaudotrainee.galeryapp

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.applaudotrainee.galeryapp.viewmodel.PhotoPreviewViewModel
import com.applaudotrainee.galleryapp.domain.interaction.GetPhotosFromApi
import com.applaudotrainee.galleryapp.domain.interaction.GetPhotosUseCase
import com.applaudotrainee.galleryapp.domain.model.PhotoPreviewEntity
import com.applaudotrainee.galleryapp.domain.util.PhotoApiStatus
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.mockito.ArgumentCaptor
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations


class PhotoPreviewViewModelTest() {

    @get:Rule
    val testInstantTaskExecutorRule: TestRule = InstantTaskExecutorRule()


    private val getPhotoApiUseCaseMock = mock(GetPhotosFromApi::class.java)
    private val getPhotosUseCaseMock = mock(GetPhotosUseCase::class.java)
    private val photoPreviewViewModel =
        PhotoPreviewViewModel(getPhotoApiUseCaseMock, getPhotosUseCaseMock)

    @Before
    fun mockFunctionality() {
        `when`(getPhotosUseCaseMock()).thenReturn(photoPreviewListFlowable)
        `when`(getPhotoApiUseCaseMock()).thenReturn(photoListFlowable)
    }

    @Test
    fun insertPhotos_success_returnsResponseFromRetrofit() {
        photoPreviewViewModel.insertPhotos().test().assertValue(photoList)
    }

    @Test
    fun getPhotoPreview_success_returnsPhotosFromDatabase() {
        photoPreviewViewModel.getPhotoPreview().test().assertValue(photoPreviewList)
    }

    @Test
    fun photoClick_success_allowGoToDetailsOnlyOnce() {
        photoPreviewViewModel.onPhotoClicked(PHOTO_ID)
        assertEquals(PHOTO_ID, photoPreviewViewModel.navigateToPhotoDetail.value)
    }

    @Test
    fun registerPhotoClick_success_cleanConfirmationArgument() {
        photoPreviewViewModel.onPhotoDetailNavigated()
        assertEquals(null, photoPreviewViewModel.navigateToPhotoDetail.value)
    }

}