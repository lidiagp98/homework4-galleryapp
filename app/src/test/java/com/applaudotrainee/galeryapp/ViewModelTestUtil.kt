package com.applaudotrainee.galeryapp

import com.applaudotrainee.galleryapp.domain.model.Photo
import com.applaudotrainee.galleryapp.domain.model.PhotoPreviewEntity
import io.reactivex.rxjava3.core.Flowable

const val PHOTO_ID = 1
const val PHOTO_ID_2 = 2
const val PHOTO_ID_3 = 3
const val ALBUM_ID = 1
const val URL_PHOTO_1 = "http://photo1.com/"
const val URL_PHOTO_2 = "http://photo2.com/"
const val URL_PHOTO_3 = "http://photo3.com/"
const val TITLE_URL_1 = "photo1"
const val TITLE_URL_2 = "photo2"
const val TITLE_URL_3 = "photo3"

val photo = Photo(ALBUM_ID, PHOTO_ID, TITLE_URL_1, URL_PHOTO_1, URL_PHOTO_1)

val photoFlowable: Flowable<Photo> = Flowable.just(photo)

val photoPreviewList = listOf(
    PhotoPreviewEntity(
        PHOTO_ID, URL_PHOTO_1, TITLE_URL_1
    ),
    PhotoPreviewEntity(
        PHOTO_ID_2, URL_PHOTO_2, TITLE_URL_2
    ),
    PhotoPreviewEntity(
        PHOTO_ID_3, URL_PHOTO_3, TITLE_URL_3
    )
)

val photoPreviewListFlowable: Flowable<List<PhotoPreviewEntity>> = Flowable.just(photoPreviewList)

val photoList = listOf(
    photo,
    Photo(
        1, PHOTO_ID_2, TITLE_URL_2, URL_PHOTO_2, TITLE_URL_2
    ),
    Photo(
        1, PHOTO_ID_2, TITLE_URL_2, URL_PHOTO_2, TITLE_URL_2
    )
)

val photoListFlowable = Flowable.just(photoList)