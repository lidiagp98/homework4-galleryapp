package com.applaudotrainee.galeryapp.binder

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.applaudotrainee.galeryapp.R
import com.applaudotrainee.galeryapp.adapter.PhotoPreviewAdapter
import com.applaudotrainee.galleryapp.domain.model.Photo
import com.applaudotrainee.galleryapp.domain.util.PhotoApiStatus
import com.applaudotrainee.galleryapp.domain.model.PhotoPreviewEntity
import com.bumptech.glide.Glide
import com.bumptech.glide.load.model.GlideUrl
import com.bumptech.glide.load.model.LazyHeaders
import com.bumptech.glide.request.RequestOptions

@BindingAdapter("loadImage")
fun loadImage(imageView: ImageView, imageUrl: String?) {
    imageUrl?.let {
        val imgUri = imageUrl.replace("https", "http")
        val glideUrl = GlideUrl(
            imgUri, LazyHeaders.Builder()
                .addHeader("User-Agent", "your-user-agent")
                .build()
        )
        Glide.with(imageView.context)
            .load(glideUrl)
            .apply(
                RequestOptions()
                    .placeholder(R.drawable.loading_animation)
                    .error(R.drawable.ic_broken_image)
            )
            .into(imageView)
    }
}

@BindingAdapter("listData")
fun bindRecyclerView(recyclerView: RecyclerView, data: List<PhotoPreviewEntity>?) {
    val adapter = recyclerView.adapter as? PhotoPreviewAdapter
    adapter?.submitList(data)
}

@BindingAdapter("photoDetailId")
fun TextView.setPhotoDetailId(photo: Photo?) {
    photo?.let {
        text = context.getString(R.string.photo_id, photo.idPhoto)
    }
}

@BindingAdapter("photoDetailAlbum")
fun TextView.setPhotoDetailAlbum(photo: Photo?) {
    photo?.let {
        text = context.getString(R.string.album_id, photo.albumId)
    }
}

@BindingAdapter("photoDetailTitle")
fun TextView.setPhotoDetailTitle(photo: Photo?) {
    photo?.let {
        text = photo.title
    }
}

@BindingAdapter("photoDetailThumb")
fun TextView.setPhotoDetailThumb(photo: Photo?) {
    photo?.let {
        text = context.getString(R.string.thumbnail, photo.thumbnailUrl)
    }
}

@BindingAdapter("photoDetailUrl")
fun TextView.setPhotoDetailUrl(photo: Photo?) {
    photo?.let {
        text = context.getString(R.string.url, photo.url)
    }
}

@BindingAdapter("photoApiStatus")
fun bindStatus(statusImageView: ImageView, status: PhotoApiStatus?) {
    when (status) {
        PhotoApiStatus.LOADING -> {
            statusImageView.visibility = View.VISIBLE
            statusImageView.setImageResource(R.drawable.loading_animation)
        }
        PhotoApiStatus.ERROR -> {
            statusImageView.visibility = View.VISIBLE
            statusImageView.setImageResource(R.drawable.ic_baseline_error_24)
        }
        PhotoApiStatus.DONE -> {
            statusImageView.visibility = View.GONE
        }
        PhotoApiStatus.NO_CONNECTION -> {
            statusImageView.visibility = View.VISIBLE
            statusImageView.setImageResource(R.drawable.ic_connection_error)
        }
    }
}

@BindingAdapter("message")
fun viewMessage(textView: TextView, status: PhotoApiStatus?) {
    when (status) {
        PhotoApiStatus.DONE -> {
            textView.visibility = View.GONE
        }
        PhotoApiStatus.NO_CONNECTION -> {
            textView.visibility = View.VISIBLE
        }
    }
}