package com.applaudotrainee.galeryapp

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil.setContentView
import com.applaudotrainee.galeryapp.databinding.ActivityPhotoDetailsBinding
import com.applaudotrainee.galeryapp.viewmodel.PhotoDetailsViewModel
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.schedulers.Schedulers
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class PhotoDetailsActivity : AppCompatActivity() {

    companion object {
        const val PHOTO_ID = "photoId"
        const val DEFAULT_PHOTO_ID_VALUE = 1
    }

    private val photoDetailsViewModel: PhotoDetailsViewModel by viewModel()

    private val disposable = CompositeDisposable()

    var photoId = DEFAULT_PHOTO_ID_VALUE

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding: ActivityPhotoDetailsBinding =
            setContentView(this, R.layout.activity_photo_details)

        photoId = intent.getIntExtra(PHOTO_ID, DEFAULT_PHOTO_ID_VALUE)

        binding.viewModelDetails = photoDetailsViewModel
        binding.lifecycleOwner = this
    }

    override fun onStart() {
        super.onStart()
        disposable.add(photoDetailsViewModel.getPhotoPreview(photoId).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                photoDetailsViewModel.photoDetails.set(it)
                photoDetailsViewModel.photoDetails.notifyChange()
            })
    }

    override fun onStop() {
        super.onStop()
        disposable.clear()
    }
}