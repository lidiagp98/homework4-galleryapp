package com.applaudotrainee.galeryapp.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.applaudotrainee.galeryapp.adapter.PhotoPreviewAdapter.PhotoPreviewViewHolder
import com.applaudotrainee.galeryapp.databinding.PhotoPreviewItemsBinding
import com.applaudotrainee.galleryapp.domain.model.PhotoPreviewEntity

class PhotoPreviewAdapter(private val clickListener: PhotoListener) :
    ListAdapter<PhotoPreviewEntity, PhotoPreviewViewHolder>(PhotoPreviewDiffCallback()) {

    class PhotoListener(val clickListener: (photoId: Int) -> Unit) {
        fun onClick(photo: PhotoPreviewEntity) = clickListener(photo.idPhoto)
    }

    class PhotoPreviewViewHolder(private var binding: PhotoPreviewItemsBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(photoPreview: PhotoPreviewEntity, clickListener: PhotoListener) {
            binding.photo = photoPreview
            binding.clickListener = clickListener
            binding.executePendingBindings()
        }
    }

    override fun onBindViewHolder(holder: PhotoPreviewViewHolder, position: Int) {
        val photoPreview = getItem(position)
        holder.bind(photoPreview, clickListener)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhotoPreviewViewHolder {
        return PhotoPreviewViewHolder(PhotoPreviewItemsBinding.inflate(LayoutInflater.from(parent.context)))
    }

    class PhotoPreviewDiffCallback : DiffUtil.ItemCallback<PhotoPreviewEntity>() {

        override fun areItemsTheSame(
            oldItem: PhotoPreviewEntity,
            newItem: PhotoPreviewEntity
        ): Boolean {
            return oldItem === newItem
        }

        override fun areContentsTheSame(
            oldItem: PhotoPreviewEntity,
            newItem: PhotoPreviewEntity
        ): Boolean {
            return (oldItem.idPhoto == newItem.idPhoto &&
                    oldItem.title == newItem.title &&
                    oldItem.thumbnailUrl == newItem.thumbnailUrl)
        }

    }
}


