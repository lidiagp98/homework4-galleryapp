package com.applaudotrainee.galeryapp.viewmodel

import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.applaudotrainee.galleryapp.domain.interaction.GetPhotosFromApi
import com.applaudotrainee.galleryapp.domain.interaction.GetPhotosUseCase
import com.applaudotrainee.galleryapp.domain.model.PhotoPreviewEntity
import com.applaudotrainee.galleryapp.domain.util.PhotoApiStatus

class PhotoPreviewViewModel(
    private val getPhotoApiUseCase: GetPhotosFromApi,
    private val getPhotosUseCase: GetPhotosUseCase
) : ViewModel() {

    var photoPreview = ObservableField<List<PhotoPreviewEntity>>()
    var status = ObservableField<PhotoApiStatus>()

    fun getPhotoPreview() = getPhotosUseCase()

    fun insertPhotos() = getPhotoApiUseCase()

    private val _navigateToPhotoDetail = MutableLiveData<Int>()
    val navigateToPhotoDetail
        get() = _navigateToPhotoDetail

    fun onPhotoClicked(photoId: Int) {
        _navigateToPhotoDetail.value = photoId
    }

    fun onPhotoDetailNavigated() {
        _navigateToPhotoDetail.value = null
    }
}