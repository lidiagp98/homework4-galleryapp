package com.applaudotrainee.galeryapp.viewmodel

import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.applaudotrainee.galleryapp.domain.interaction.GetPhotoDetailsUseCase
import com.applaudotrainee.galleryapp.domain.model.Photo

class PhotoDetailsViewModel(
    private val getPhotoDetailsUseCase: GetPhotoDetailsUseCase
) : ViewModel() {

    val photoDetails = ObservableField<Photo>()

    fun getPhotoPreview(photoId: Int) = getPhotoDetailsUseCase(photoId)
}