package com.applaudotrainee.galeryapp

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkRequest
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import com.applaudotrainee.galeryapp.adapter.PhotoPreviewAdapter
import com.applaudotrainee.galeryapp.databinding.ActivityMainBinding
import com.applaudotrainee.galeryapp.viewmodel.PhotoPreviewViewModel
import com.applaudotrainee.galleryapp.domain.util.PhotoApiStatus
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.schedulers.Schedulers
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity() {

    companion object {
        const val SPAN_COUNT = 2
    }

    private val photoViewModel: PhotoPreviewViewModel by viewModel()

    private lateinit var binding: ActivityMainBinding

    private val disposable = CompositeDisposable()

    private lateinit var photoAdapter: PhotoPreviewAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(
            this, R.layout.activity_main
        )
        binding.lifecycleOwner = this
        binding.viewModel = photoViewModel
        photoAdapter = PhotoPreviewAdapter(PhotoPreviewAdapter.PhotoListener { photoId ->
            photoViewModel.onPhotoClicked(photoId)
        })

        binding.photoPreviewRecycler.adapter = photoAdapter

        val layoutManager = GridLayoutManager(this, SPAN_COUNT)
        binding.photoPreviewRecycler.layoutManager = layoutManager

        startNetworkCallback()

        photoViewModel.navigateToPhotoDetail.observe(this, Observer { photoId ->
            photoId?.let {
                val intent = Intent(this, PhotoDetailsActivity::class.java).apply {
                    putExtra(PhotoDetailsActivity.PHOTO_ID, photoId)
                }
                startActivity(intent)
                photoViewModel.onPhotoDetailNavigated()
            }
        })
    }

    override fun onStart() {
        super.onStart()
        requestPhotosFromApi()
        getPhotosFromDatabase()
    }

    private fun requestPhotosFromApi() {
        if (photoViewModel.photoPreview.get().isNullOrEmpty()) {
            disposable.add(photoViewModel.insertPhotos().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnComplete {
                    photoViewModel.status.set(PhotoApiStatus.DONE)
                    photoViewModel.status.notifyChange()
                }
                .doOnError {
                    photoViewModel.status.set(PhotoApiStatus.ERROR)
                    photoViewModel.status.notifyChange()
                }
                .onErrorReturn {
                    listOf()
                }
                .subscribe())
        }
    }

    private fun getPhotosFromDatabase() {
        disposable.add(photoViewModel.getPhotoPreview().subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                if (it != photoViewModel.photoPreview.get()) {
                    photoViewModel.photoPreview.set(it)
                    photoViewModel.photoPreview.notifyChange()
                }
            })
    }


    private fun startNetworkCallback() {
        val cm: ConnectivityManager =
            application.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val builder: NetworkRequest.Builder = NetworkRequest.Builder()

        cm.registerNetworkCallback(
            builder.build(),
            object : ConnectivityManager.NetworkCallback() {

                override fun onAvailable(network: Network) {
                    photoViewModel.status.set(PhotoApiStatus.DONE)
                    photoViewModel.status.notifyChange()
                }

                override fun onLost(network: Network) {
                    photoViewModel.status.set(PhotoApiStatus.NO_CONNECTION)
                    photoViewModel.status.notifyChange()
                }
            })
    }

    override fun onStop() {
        super.onStop()
        disposable.clear()
    }
}
