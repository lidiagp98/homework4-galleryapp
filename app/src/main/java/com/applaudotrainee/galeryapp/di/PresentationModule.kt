package com.applaudotrainee.galeryapp.di

import com.applaudotrainee.galeryapp.viewmodel.PhotoDetailsViewModel
import com.applaudotrainee.galeryapp.viewmodel.PhotoPreviewViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val presentationModule = module {
    viewModel { PhotoPreviewViewModel(get(), get()) }
    viewModel { PhotoDetailsViewModel(get()) }
}