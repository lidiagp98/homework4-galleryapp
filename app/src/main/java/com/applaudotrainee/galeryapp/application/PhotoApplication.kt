package com.applaudotrainee.galeryapp.application

import android.app.Application
import com.applaudotrainee.galeryapp.di.presentationModule
import com.applaudotrainee.galleryapp.data.di.databaseModule
import com.applaudotrainee.galleryapp.data.di.repositoryModule
import com.applaudotrainee.galleryapp.data.di.serviceModule
import com.applaudotrainee.galleryapp.domain.di.interactionModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class PhotoApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@PhotoApplication)
            modules(appModules + domainModules + dataModules)
        }
    }
}

val appModules = listOf(presentationModule)
val domainModules = listOf(interactionModule)
val dataModules = listOf(databaseModule, repositoryModule, serviceModule)